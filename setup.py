from setuptools import setup


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="arxify",
    version="0.1.0",
    description="Prepares arXiv submission from pdlatex+bibtex project.",
    long_description=long_description,
    packages=["arxify"],
    author=["Thomas Biekotter"],
    author_email=["thomas.biekoetter@desy.de"],
    url="https://gitlab.com/thomas.biekoetter/arxify",
    include_package_data=True,
    package_data={"plotius": ["*"]},
    install_requires=["numpy"],
    python_requires='>=3.6',
)
