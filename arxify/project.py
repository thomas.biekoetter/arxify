import os
import shutil


class Project:

    def __init__(self, path_to_folder, remove_comments=True):

        self.temp_suffix = '_temp_from_arxify'

        self.path_to_folder = self.prep_path(path_to_folder)
        self.path_output = self.prep_path_out(self.path_to_folder)
        self.targz_name = self.prep_targz_name(self.path_output)

        try:
            os.mkdir(self.path_output)
        except FileExistsError:
            shutil.rmtree(self.path_output)
            os.mkdir(self.path_output)

        print('-- Path to project: ' + self.path_to_folder)

        self.all_files = self.read_all_files(self.path_to_folder)
        self.all_tex, self.main_tex = self.find_tex_files(self.all_files)
        main_print = self.main_tex.split(self.path_to_folder + '/')[1]
        self.main_tex_name = main_print.split('.tex')[0]
        print('-- The main tex file is: ' + main_print)

        self.secondary_texs = self.find_secondary_tex_files()
        aux_print = [fl.split(self.path_to_folder + '/')[1] for fl in self.secondary_texs]
        print('-- Auxailiary tex files are: ' + str(aux_print))

        if remove_comments:
            print('-- Comments from tex files will be removed (default).')
        else:
            print('-- Comments from tex files will not be removed.')
        self.place_tex_files(remove_comments)

        self.place_bibtex_stylefile()
        try:
            bib_print = self.bibfile.split(self.path_to_folder + '/')[1]
            print('-- The bibtex stylefile is: ' + bib_print)
        except AttributeError:
                print(
                    '-- The bibtex style is ' + self.bibstyle +
                    '.bst, but stylefile not found in directory. ' +
                    'Please check if it is required or part of ' +
                    'the standard tex environment.')

        self.place_bibtex_literature()
        bib_print = self.biblit.split(self.path_to_folder + '/')[1]
        print('-- The bibtex file is: ' + bib_print)

        self.place_graphics()
        graphics_print = str(self.graphics)
        print(
            '-- In total there were ' + str(len(self.graphics)) +
            ' graphic files: ' + graphics_print)

        self.place_latex_packages()
        pack_print = [x.split(self.path_to_folder + '/')[1] for \
            x in self.latex_stylefiles]
        print(
            '-- The following latex packages were found: ' +
            str(pack_print))

        self.place_latex_class()
        try:
            class_print = self.latex_class.split(self.path_to_folder + '/')[1]
            print('-- The latex class is: ' + class_print)
        except IndexError:
            if not self.latex_class in 'article':
                print(
                    '-- The latex class is ' + self.latex_class +
                    ', but the class file was not found in directory. ' +
                    'Please check if it is required or part of ' +
                    'the standard tex environment.')
            else:
                print('-- The latex class is: ' + self.latex_class)

    def prep_path(self, pt):

        if os.path.isdir(pt):
            if pt[-1] == '/':
                pt = pt[0:-1]
        else:
            raise NameError(
                'Path belongs to a file or does not exist. ' +
                'Please provide path to folder.')

        return pt

    def prep_path_out(self, pt):

        pt = pt.split('/')[-1]
        pt = pt + self.temp_suffix

        return pt

    def prep_targz_name(self, pt):

        pt = pt.split(self.temp_suffix)[0]
        pt = pt + '.tar.gz'

        return pt

    def read_all_files(self, pt):

        all_files = []
        for path, subdirs, files in os.walk(pt):
            for name in files:
                all_files.append(os.path.join(path, name))

        return all_files

    def find_tex_files(self, pts):

        all_texs = []
        for pt in pts:
            if pt.split('.')[-1] == 'tex':
                all_texs.append(pt)

        main_texs = []
        for pt in all_texs:
            with open(pt) as f:
                if '\\begin{document}' in f.read():
                    main_texs.append(pt)

        if len(main_texs) != 1:
            raise RuntimeError(
                'No main tex file identified. ' +
                'Please check if the input path is correct.')

        return all_texs, main_texs[0]

    def find_secondary_tex_files(self):

        with open(self.main_tex) as f:
            main_lines = f.readlines()

        secondaries = []
        for fl in self.all_tex:
            if fl == self.main_tex:
                continue
            aux_name = fl.split('/')[-1].split('.tex')[0]
            for ln in main_lines:
                # Check if line contains filename
                if aux_name in ln:
                    # Check if comment line, if not add to secondary files
                    pre = ln.split(fl)[0]
                    if not '%' in pre:
                        if not '\\%' in pre:
                            secondaries.append(fl)
        return secondaries

    def place_tex_files(self, rem_com):

        self.secondary_texs_outname = []

        for i, tex in enumerate([self.main_tex] + self.secondary_texs):
            with open(tex) as f:
                lns = f.readlines()
            if rem_com:
                lns = self.remove_comments_from_tex(lns)
            out_name = self.path_output + '/' + tex.split(self.path_to_folder + '/')[1]
            if i == 0:
                self.main_tex_outname = out_name
            else:
                self.secondary_texs_outname.append(out_name)
            with open(out_name, 'w') as f:
                f.writelines(lns)

    def remove_comments_from_tex(self, lns):

        clean = []
        for ln in lns:
            if not '\\%' in ln:
                if len(ln.split('%')) > 1:
                    cl = ln.split('%')[0]
                    # Put linebreak back in if not whole line a comment
                    if len(ln.split('%')[0].replace(' ', '')) > 0:
                        cl += '\n'
                else:
                    cl = ln
            else:
                cl = ln
            clean.append(cl)

        return clean

    def place_bibtex_stylefile(self):

        found = False
        for tex in [self.main_tex_outname] + self.secondary_texs_outname:

            with open(tex) as f:
                lns = f.readlines()

            for ln in lns:
                if not '\\%' in ln:
                    cl = ln.split('%')[0]
                else:
                    cl = ln
                if '\\bibliographystyle' in cl:
                    self.bibstyle = cl.split('{')[1].split('}')[0]
                    found = True
                    break

            if found:
                break

        if found:
            for fl in self.all_files:
                if self.bibstyle + '.bst' in fl:
                    self.bibfile = fl

            try:
                self.bibfile_outname = self.path_output + '/' + \
                    self.bibfile.split(self.path_to_folder + '/')[1]

                shutil.copyfile(
                    self.bibfile,
                    self.bibfile_outname)
            except AttributeError:
                pass

    def place_bibtex_literature(self):

        found = False
        for tex in [self.main_tex_outname] + self.secondary_texs_outname:

            with open(tex) as f:
                lns = f.readlines()

            for ln in lns:
                if not '\\%' in ln:
                    cl = ln.split('%')[0]
                else:
                    cl = ln
                if '\\bibliography{' in cl:
                    biblit = ln.split('{')[1].split('}')[0]
                    # Take away file ending in case
                    biblit = biblit.split('.bib')[0]
                    found = True
                    break

            if found:
                break

        for fl in self.all_files:
            if biblit + '.bib' in fl:
                self.biblit = fl
                break

        self.biblit_outname = self.path_output + '/' + \
            self.biblit.split(self.path_to_folder + '/')[1]

        shutil.copyfile(
            self.biblit,
            self.biblit_outname)

    def place_graphics(self):

        # Find graphics paths
        self.graphics_paths = []

        for tex in [self.main_tex_outname] + self.secondary_texs_outname:

            with open(tex) as f:
                lns = f.readlines()

            for ln in lns:
                try:
                    extr = ln.split('#')[1]
                except IndexError:
                    extr = ln
                if '\\graphicspath{' in extr:
                    extr = extr.split('{', 1)[1]
                    extr = extr.split('}}')[0] + '}'
                    extr = [x.replace('{', '') for x in extr.split('}')][0:-1]
                    self.graphics_paths += extr

        # Check that no parents or siblings, otherwise make subdir in case needed
        for pt in self.graphics_paths:
            if '..' in pt[0:2]:
                raise RuntimeError(
                    'All graphic files must be inside the ' +
                    'project path ' + self.path_to_folder + ' ' +
                    'or subdirectories therein. Cannot handle: ' +
                    ' \'\\graphicspath{../something}\'.')
            else:
                try:
                    os.mkdir(self.path_output + '/' + pt)
                except FileExistsError:
                    pass

        # Find graphics and place them
        self.graphics = []

        for tex in [self.main_tex_outname] + self.secondary_texs_outname:

            with open(tex) as f:
                lns = f.readlines()

            for ln in lns:
                try:
                    extr = ln.split('#')[1]
                except IndexError:
                    extr = ln
                if '\\includegraphics' in ln:
                    passed_include = False
                    for ex in extr.split('includegraphics')[1:]:
                        if '{' in ex:
                            itm = ex
                            itm = itm.split('{')[1]
                            itm = itm.split('}')[0]
                            self.graphics.append(itm)

        # Add ".pdf" as file ending if no file ending given
        was_raised = False
        grlist = []
        for gr in self.graphics:
            grfl = gr.split("/")[-1]
            if "." not in grfl:
                gr = gr + ".pdf"
                if not was_raised:
                    print()
                    print("Warning: tex file contains \includegraphics{...}")
                    print("where the filename of the graphic does")
                    print("not contain the file ending.")
                    print("  --> Assuming that the file ending is '.pdf'.")
                    print()
                    was_raised = True
            grlist.append(gr)
        self.graphics = grlist

        self.graphics_orig = []
        founders = list(range(0, len(self.graphics)))

        for fl in self.all_files:

            for i, gr in enumerate(self.graphics):

                # fls = fl fl.split('/')[-1]
                fls = ''
                for x in fl.split('/')[1:]:
                    fls += x
                    fls += '/'
                fls = fls[:-1]

                grs = [gr]
                for pt in self.graphics_paths:
                    grs.append(pt + gr)
                # if (gr == fls) or ():
                for g in grs:
                    if g == fls:
                        founders.remove(i)
                        self.graphics_orig.append(fl)

        for i in founders:

            raise RuntimeError(
                'The following graphic file missing: ' +
                self.graphics[i])

        self.graphics_outnames = []
        for gr in self.graphics_orig:

            self.graphics_outnames.append(self.path_output + '/' + \
                gr.split(self.path_to_folder + '/')[1])

        for f1, f2 in zip(
            self.graphics_orig,
            self.graphics_outnames):

            try:
                shutil.copyfile(f1, f2)
            except FileNotFoundError:
                # There are subdirectories, have to create them first
                subs = f2.split('/')[0:-1]
                newdir = ''
                for s in subs:
                    newdir += s
                    newdir += '/'
                    try:
                        os.mkdir(newdir)
                    except FileExistsError:
                        pass
                shutil.copyfile(f1, f2)

        # Delete subfolders if empty
        for pt in self.graphics_paths:

            try:
                os.rmdir(self.path_output + '/' + pt)
            except:
                pass

    def place_latex_packages(self):

        self.latex_stylefiles = []
        for fl in self.all_files:

            # Check if package files in directory
            if '.sty' in fl:

                # Check if actually used
                name = fl

                for tex in [self.main_tex_outname] + \
                    self.secondary_texs_outname:

                    with open(tex) as f:
                        lns = f.readlines()

                    for ln in lns:

                        if not '\\%' in ln:
                            cl = ln.split('%')[0]
                        else:
                            cl = ln

                        if '\\usepackage{' + \
                            name.split('/')[-1].split('.sty')[0] + '}' in cl:
                            self.latex_stylefiles.append(name)

        self.latex_stylefiles_outnames = []
        for st in self.latex_stylefiles:
            self.latex_stylefiles_outnames.append(self.path_output + '/' + \
                st.split(self.path_to_folder + '/')[1])

        for f1, f2 in zip(
            self.latex_stylefiles,
            self.latex_stylefiles_outnames):
            shutil.copyfile(f1, f2)

    def place_latex_class(self):

        found = False

        for fl in self.all_files:

            # Check if class files in directory
            if ('.cls' in fl) or ('.clo' in fl):

                # Check if actually used
                name = fl

                for tex in [self.main_tex_outname] + \
                    self.secondary_texs_outname:

                    with open(tex) as f:
                        lns = f.readlines()

                    for ln in lns:

                        if not '\\%' in ln:
                            cl = ln.split('%')[0]
                        else:
                            cl = ln

                        if '\\documentclass' in cl:
                            if name.split('/')[-1].split('.')[0] in cl:

                                self.latex_class = name
                                found = True
                                break

                    if found:
                        break

        if found:

            self.latex_class_outname = self.path_output + '/' + \
                self.latex_class.split(self.path_to_folder + '/')[1]

            shutil.copyfile(
                self.latex_class,
                self.latex_class_outname)

        else:

           for tex in [self.main_tex_outname] + \
               self.secondary_texs_outname:

               with open(tex) as f:
                   lns = f.readlines()

               for ln in lns:

                   if not '\\%' in ln:
                       cl = ln.split('%')[0]
                   else:
                       cl = ln

                   if '\\documentclass' in cl:

                        extr = cl.split('{')[1].split('}')[0]
                        self.latex_class = extr

    def remove_bib_file(self):

        # Check if bbl file is there, if yes delete bib file
        self.bbl_name = self.path_output + '/' + \
            self.main_tex_name + '.bbl'

        if os.path.isfile(self.bbl_name):
            os.remove(self.biblit_outname)
        else:
            raise RuntimeError(
                'Cannot find bbl file.')

    def clean_please(self):

        shutil.rmtree(self.path_output)
