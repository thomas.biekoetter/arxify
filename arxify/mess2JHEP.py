import sys

from arxify.project import Project
from arxify.runner import Runner
from arxify.targzer import folder2targz


if sys.version_info[0] < 3:
    raise Exception('Python v.2 is not supported.')


def main(argv):

    try:
        INPUT = argv[0]
    except:
        raise RuntimeError(
            'Invalid input path.')

    try:
        OUTNAME = argv[1]
        if (OUTNAME == 'False') or (OUTNAME == 'True'):
            raise RuntimeError(
                'You cannot use True or False for output names.')
    except IndexError:
        raise RuntimeError(
            'Please provide an output name.')

    try:
        REMOVE_COMM = argv[2]
    except IndexError:
        REMOVE_COMM = 'True'
    if 'True' in REMOVE_COMM:
        REMOVE_COMM = True
    elif 'False' in REMOVE_COMM:
        REMOVE_COMM = False
    else:
        raise RuntimeError(
            'Invalid flag for removing comments option.')

    print()
    print('Extracting relevant files:')
    pro = Project(
        INPUT,
        remove_comments=REMOVE_COMM)

    print()
    print('Trying to compile the project:')
    try:
        run = Runner(
            pro.path_output,
            pro.main_tex_name)
    except RuntimeError:
        print(
            '-- Test compilation not succesful. ' +
            'Check this on your own.')

    print()
    print('Creating tar.gz file for submission.')
    try:
        pro.remove_bib_file()
        print('-- bibtex file is removed.')
    except:
        raise
    try:
        folder2targz(pro.path_output, OUTNAME, mode="JHEP")
    except RuntimeError:
        print(
            '-- Cannot create tar.gz for submission. ' +
            'Is \'tar\' installed?')
    print('-- tar.gz file created succesfully.')

    try:
        pro.clean_please()
        print('-- Removed temporary files.')
    except:
        raise

    print()
    print('Submission saved in: ' + OUTNAME + '.tar.gz')


if __name__ == '__main__':
    main(sys.argv[1:])
