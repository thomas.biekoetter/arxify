import os
import shutil
import subprocess

TAR_TIME = 4


def folder2targz(direc, name, mode='arXiv'):

    try:
        shutil.copytree(direc, name)
    except FileExistsError:
        shutil.rmtree(name)
        shutil.copytree(direc, name)

    if mode == 'arXiv':
        command = 'tar -zcvf ' + name + \
            '.tar.gz ' + name + '/'
    elif mode == "JHEP":
        command = 'tar -cvzf ' + name + '.tar.gz *'
    else:
        raise ValueError('Wrong argument for mode (arXiv or JHEP)')

    try:
        if mode == 'arXiv':
            output = subprocess.run(
                command.split(),
                stdout=subprocess.PIPE,
                timeout=TAR_TIME)
        elif mode == 'JHEP':
            os.chdir(name)
            # Use here shell=True because otherwise wildcard * not working
            output = subprocess.run(
                command,
                stdout=subprocess.PIPE,
                timeout=TAR_TIME,
                shell=True)
            command = 'mv ' + name + '.tar.gz ../'
            subprocess.run(
                command.split(),
                stdout=subprocess.PIPE,
                timeout=TAR_TIME)
            os.chdir('..')
    except subprocess.TimeoutExpired:
        raise RuntimeError(
            'Compressing folder went wrong.')
    except FileNotFoundError:
        raise RuntimeError(
            'Compressing folder went wrong.')

    shutil.rmtree(name)
