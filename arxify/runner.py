import os
import shutil
import subprocess


class Runner:

    def __init__(self, path, main_tex):

        self.LATEX_TIME = 14
        self.BIBTEX_TIME = 4

        self.check_compilers()
        print('-- pdflatex and bibtex compilers are installed.')

        test_path = path + '_comp'

        try:
            shutil.copytree(path, test_path)
        except FileExistsError:
            shutil.rmtree(test_path)
            shutil.copytree(path, test_path)

        os.chdir(test_path)

        self.run_pdflatex(main_tex)
        self.run_pdflatex(main_tex)
        print('-- pdflatex compiled successfully.')

        self.run_bibtex(main_tex)
        print('-- bibtex compiled successfully.')

        # Just to be sure that PDF is fine
        self.run_bibtex(main_tex)
        self.run_pdflatex(main_tex)
        self.run_pdflatex(main_tex)

        # Copy bbl file to path
        shutil.copyfile(
            self.bbl_file,
            '../' + path + '/' + self.bbl_file)
        print('-- bibtex bbl file saved.')

        # Go back and clean up
        os.chdir('..')
        shutil.rmtree(test_path)

    def check_compilers(self):

        command = 'pdflatex --version'

        try:
            process = subprocess.Popen(
                command.split(),
                stdout=subprocess.PIPE)
            output, error = process.communicate()
        except FileNotFoundError:
            raise RuntimeError(
                'pdflatex is not installed on your system. ' +
                'Test compilation cannot be carried out.')

        command = 'bibtex --version'

        try:
            process = subprocess.Popen(
                command.split(),
                stdout=subprocess.PIPE)
            output, error = process.communicate()
        except FileNotFoundError:
            raise RuntimeError(
                'bibtex is not installed on your system. ' +
                'Test compilation cannot be carried out.')

    def run_pdflatex(self, tex):

        command = 'pdflatex ' + tex + '.tex'

        try:
            output = subprocess.run(
                command.split(),
                stdout=subprocess.PIPE,
                timeout=self.LATEX_TIME)
        except subprocess.TimeoutExpired:
            raise RuntimeError(
                'pdflatex compilation went wrong.')

        # Check that there is a pdf
        found = False
        for fl in os.listdir():
            if tex + '.pdf' in fl:
                found = True
                break
        if not found:
            raise RuntimeError(
                'pdflatex compilation went wrong.')

    def run_bibtex(self, tex):

        command = 'bibtex ' + tex + '.aux'

        try:
            output = subprocess.run(
                command.split(),
                stdout=subprocess.PIPE,
                timeout=self.BIBTEX_TIME)
        except subprocess.TimeoutExpired:
            raise RuntimeError(
                'bibtex compilation went wrong.')

        # Check that there is a bbl
        found = False
        for fl in os.listdir():
            if '.bbl' in fl:
                found = True
                self.bbl_file = fl
                break
        if not found:
            raise RuntimeError(
                'bibtex compilation went wrong.')
