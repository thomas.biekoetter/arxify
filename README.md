# arxify

Extracts the required files for an arXiv submission
from an existing pdflatex+bibtex project. For tex
files one can optionally remove all comments from
the files. The final submission will be saved as
a tar.gz file.

It is required that the files can be identified
by their usual file endings: `tex` for pdflatex
files, `bib` for the bibtex file, `bst` for the
bibtex style file, `cls` or `clo` for the latex
class, and `sty` for latex packages.

## Disclaimer

Use this code at your own risk.

I do not take any responsibility for your submissions
to the arXiv. I developed this code based on my own
projects. There are probably some peculiar things I
have not thought of.

If you encounter any problems, please let me know!

## Installation

Using pip:
```
python -m pip install --user .
```

Not using pip:
```
python setup.py install --user
```

## Usage

Just type:
```
python -m arxify.mess2arXiv /path/to/project/folder/ [outputname] [True/False]
```
The argument `outputname` will be the name of the
final `.tar.gz` file that you can submit to
the arXiv. The third argument defines whether
the comments of the tex files shall be removed.
It can be set to `True` or `False`.

There cannot be more than one tex files
including the line `\begin{document}` in
the directory and all subdirectories.
Otherwise it is impossible to know which
is the project you want to submit.

Some journals cannot handle submissions in which
the main tex file is placed in a directory within
the tar.gz file. This limitation concerns, for instance,
JHEP (and probably others). In order to create a file
that can be submitted to such journals, one can use
```
python -m arxify.mess2JHEP /path/to/project/folder/ [outputname] [True/False]
```
The arguments are the same as explained above for
the `mess2arXiv` command.
